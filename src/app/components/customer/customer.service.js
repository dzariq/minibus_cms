/*
 * This file is part of EMCOO TEAM PROJECT.
 *
 *  (c) EMCOO TEAM
 *  (c) DevMark <mark@emcoo.com>
 *
 *  For the full copyright and license information, please view http://emcoo.com/
 *
 *
 */
(function () {
    'use strict';

    angular.module('customerModule').factory('customerService', userService);
    function userService($http, localStorageService,$q) {

        var service = {};

        service.get = function () {
            var m = $q.defer();
            $http({
                url: http_path + 'users',
                method: "GET",
                headers: {
                    'Authorization': 'Bearer '+localStorageService.get('token', 0)
                }
            }).success(function (response) {
                m.resolve(response);
            })
            return m.promise;
        };
        service.find = function (id, httpConfig) {
          
        };
        service.update = function (id, data) {
        };
        service.store = function (data) {
        };
        service.destroy = function (id) {
        };


        return service;

    }
})();
