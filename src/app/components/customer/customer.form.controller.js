/*
 * This file is part of EMCOO TEAM PROJECT.
 *
 *  (c) EMCOO TEAM
 *  (c) DevMark <mark@emcoo.com>
 *
 *  For the full copyright and license information, please view http://emcoo.com/
 *
 *
 */
(function () {
    'use strict';

    angular.module('customerModule').controller('CustomerFormController', CustomerFormController);
    function CustomerFormController($scope, userService, messageService, toaster, $translate, customer, $location, $q, roleService, $state) {
        var vm = this;

        //==========================================
        // Variable
        //==========================================
        vm.customer = (_.isEmpty(customer) || _.isUndefined(customer)) ? userService.init() : customer;


        //==========================================
        // Function
        //==========================================
        vm.roles = [];
        vm.refreshRole = function (string) {
            if (string !== '') {
                roleService.get({search: string}).then(function (result) {
                    vm.roles = result;
                });
            }
        };

        //==========================================
        // save
        //==========================================
        var save = function () {
            var customer = angular.copy(vm.customer);
            customer.roles = _.pluck(customer.roles, 'id');
            var deferred = $q.defer();
            if (customer.id !== '') {
                userService.update(customer.id, customer).then(function (result) {
                    deferred.resolve(result);
                }, function (result) {
                    deferred.reject(result);
                });
            } else {
                userService.store(customer).then(function (result) {
                    deferred.resolve(result);
                }, function (result) {
                    deferred.reject(result);
                });
            }

            return deferred.promise;

        };

        vm.isSaveAndExit = false;
        vm.saveLoading = false;
        vm.save = function () {
            vm.saveLoading = true;
            save().then(function (result) {
                vm.saveLoading = false;
                toaster.pop('success', '', $translate.instant('customer.' + (vm.customer.id !== '' ? 'update_success_msg' : 'create_success_msg')));
                if (vm.isSaveAndExit) {
                    $state.go('main.customer-list');
                } else if (vm.customer.id === '') {
                    $state.go('main.customer-edit', {id: result.id});
                } else {
                    vm.customer = result;
                }
            }, function (result) {
                vm.saveLoading = false;
                messageService.formError(result);
            });
        };

        vm.saveAndExit = function () {
            vm.isSaveAndExit = true;
            vm.save();
        };

        //==========================================
        // Delete
        //==========================================
        vm.deleteLoading = false;
        vm.delete = function () {
            if (vm.customer.id !== '') {
                vm.deleteLoading = true;
                userService.destroy(vm.customer.id).then(function () {
                    toaster.pop('success', '', $translate.instant('customer.delete_success_msg'));
                    $location.path('customers');
                }, function (result) {
                    vm.deleteLoading = false;
                    toaster.pop('success', '', $translate.instant('customer.delete_error_msg'));
                });
            }

        };

    }
})();
